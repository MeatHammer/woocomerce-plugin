<?php
/**
 * Plugin Name: WooCommerce Custom Products
 * Description: Custom products for woocommerce
 * License: GPLv3 or later License
 * URI: https://gitlab.com/MeatHammer/woocomerce-plugin
 * Version: 1.0.0
 */

defined( 'ABSPATH' ) || exit;

if (!defined( 'WC_CUSTOM_PLUGIN_FILE')){
    define('WC_CUSTOM_PLUGIN_FILE', __FILE__ );
}

if (in_array( 'woocommerce/woocommerce.php', apply_filters('active_plugins', get_option('active_plugins')))){
    if (!class_exists( 'WoocommerceCustomProduct')){
        include_once dirname( WC_CUSTOM_PLUGIN_FILE ) . '/includes/custom_product.php';
    }
}
?>
