<?php

function register_custom_product(){
    class WoocommerceCustomProduct extends WC_Product {
        public function __construct( $product ) {
            $this->product_type = 'Individual';
            parent::__construct( $product );   
        }
    }
}
add_action( 'init', 'register_custom_product' );


function wdm_add_custom_product_type( $types ){
    $types[ 'custom_product' ] = __( 'Individual' );
    return $types;
}
add_filter( 'product_type_selector', 'wdm_add_custom_product_type' );



function custom_product_tabs( $tabs) {

	$tabs['custom_product'] = array(
		'label'		=> __( 'Additonal Setting', 'woocommerce' ),
		'target'	=> 'custom_product_options',
		'class'		=> array( 'show_if_custom_product'),
	);

	return $tabs;

}
add_filter( 'woocommerce_product_data_tabs', 'custom_product_tabs' );

function custom_product_options_product_tab_content() {

	global $post;
	?><div id='custom_product_options' class='panel woocommerce_options_panel'><?php

		?><div class='options_group'><?php

			woocommerce_wp_checkbox( array(
				'id' 		=> '_unique_packaging_option',
				'label' 	=> __( 'Add uniqueness to the product', 'woocommerce' ),
			) );
		?></div>

	</div><?php
}
add_action( 'woocommerce_product_data_panels', 'custom_product_options_product_tab_content' );

function save_custom_option_field( $post_id ) {

	$rental_option = isset( $_POST['_unique_packaging_option'] ) ? 'yes' : 'no';
	update_post_meta( $post_id, '_unique_packaging_option', $rental_option );
}
add_action( 'woocommerce_process_product_meta_custom_product', 'save_custom_option_field'  );
add_action( 'woocommerce_process_product_meta_custom_product', 'save_custom_option_field'  );

?>